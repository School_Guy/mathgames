package de.schoolguy;

/**
 * This interface specifies what methods it should have to be operatable by this CLI.
 */
public interface Game {

    /**
     * This method starts the game. The Game should initialize arguments elsewhere. No calculations to solve a game
     * should be made before this is called.
     */
    void run();
}
