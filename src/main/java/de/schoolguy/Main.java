package de.schoolguy;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import de.schoolguy.cli.Args;
import de.schoolguy.computewall.ComputewallGame;
import de.schoolguy.crossnumbers.CrossnumbersGame;
import de.schoolguy.hackfleisch.HackfleischGame;
import de.schoolguy.maxsubstring.MaxSubStringGame;
import de.schoolguy.stringreplacementaddition.StringReplacementGame;
import de.schoolguy.unkown.UnknownGame;
import de.schoolguy.wheatchessbordproblem.WheatChessboardGame;

public class Main {

    /**
     * @param args Main program arguments for this collection of algorithms.
     */
    public static void main(String[] args) {
        Args arguments = parseArguments(args);
        GameType type = arguments.gameType;
        Game game = getGameObject(type);
        game.run();
    }

    /**
     * Parses the arguments of the program and afterwards presents them in an internally fashionable manner.
     *
     * @param args The passed through arguments from the main program method.
     * @return The Args instance with all parsed options.
     */
    private static Args parseArguments(String[] args) {
        Args arguments = new Args();
        JCommander jct = JCommander
                .newBuilder()
                .addObject(arguments)
                .build();
        try {
            jct.parse(args);
        } catch (ParameterException e) {
            e.usage();
            System.exit(1);
        }
        return arguments;
    }

    /**
     * Get an instance of a Game to play and solve it.
     *
     * @param type The type of Game.
     * @return An instance of the game.
     * @throws IllegalArgumentException Only thrown when the type is not one of the defined ones.
     */
    private static Game getGameObject(GameType type) {
        switch (type) {
            case COMPUTEWALL: {
                return new ComputewallGame();
            }
            case CROSSNUMBERS: {
                return new CrossnumbersGame();
            }
            case HACKFLEISCH: {
                return new HackfleischGame();
            }
            case MAXSUBSTRING: {
                return new MaxSubStringGame();
            }
            case STRINGREPLACEMENTADDITION: {
                return new StringReplacementGame();
            }
            case UNKOWN: {
                return new UnknownGame();
            }
            case WHEATCHESSBOARDPROBLEM: {
                return new WheatChessboardGame();
            }
            default: {
                throw new IllegalArgumentException("Unknown Game-Type selected!");
            }
        }
    }
}
