package de.schoolguy.unkown;

import de.schoolguy.Game;

/**
 * This game takes a nine digit number and divides it by a three digit number the result needs to be a six digit number
 * which then gets divided through a two digit number. The final result need to be a five digit number.
 *
 * All steps need to be divisions without any fractions of a number. The name of it is unknown, as well as its
 * GC-number.
 */
public class UnknownGame implements Game {

    //ABCDEFGHI/JKL=MNOPQR/ST=UVWXY

    /**
     * Runs the game. No threading and no optimizations were made.
     */
    @Override
    public void run() {
        int ersteZahl;
        int ersterDivisor;
        int erstesErgebnis;
        int[] erstesErgebnisArray = new int[6];
        int zweiterDivisor;
        int endergebnis = 10000;

        for (ersteZahl = 100000000; ersteZahl <= 999999999; ersteZahl++) {
            if (ersteZahl % 100000 == 0) {
                System.out.println("ersteZahl: " + ersteZahl);
            }
            for (ersterDivisor = 100; ersterDivisor <= 999; ersterDivisor++) {
                for (erstesErgebnis = 100000; erstesErgebnis <= 999999; erstesErgebnis++) {
                    if (erstesErgebnis % 1000 == 0) {
                        System.out.println("erstesErgebnis: " + erstesErgebnis);
                    }
                    for (zweiterDivisor = 10; zweiterDivisor <= 99; zweiterDivisor++) {
                        if (ersteZahl % ersterDivisor == 0) {
                            erstesErgebnis = ersteZahl / ersterDivisor;
                            if (erstesErgebnis % zweiterDivisor == 0) {
                                String s = "" + erstesErgebnis;
                                for (int i = 1; i < s.length() - 1; i++) {
                                    erstesErgebnisArray[i] = Integer.parseInt("" + s.charAt(i));
                                }
                                int klErgebnis = erstesErgebnisArray[0] * 10 + erstesErgebnisArray[1];
                                if (klErgebnis / zweiterDivisor > 0 && klErgebnis / zweiterDivisor <= 9) {
                                    endergebnis = erstesErgebnis / zweiterDivisor;
                                    if (endergebnis > 10000 && endergebnis < 99999)
                                        System.out.println(ersteZahl + " " + ersterDivisor + " " + erstesErgebnis + " "
                                                + zweiterDivisor + " " + endergebnis + " " + "klErgebnis: " + klErgebnis);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
