package de.schoolguy;

/**
 * For each Java package, except the cli, in this enum there should be a constant for.
 */
public enum GameType {
    COMPUTEWALL,
    CROSSNUMBERS,
    HACKFLEISCH,
    MAXSUBSTRING,
    STRINGREPLACEMENTADDITION,
    UNKOWN,
    WHEATCHESSBOARDPROBLEM;

    // converter that will be used later
    public static GameType fromString(String code) {
        for(GameType output : GameType.values()) {
            if(output.toString().equalsIgnoreCase(code)) {
                return output;
            }
        }
        return null;
    }
}
