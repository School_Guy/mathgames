package de.schoolguy.crossnumbers;

public class Worker implements Runnable {
    private State state;

    public Worker(State state) {
        this.state = state;
    }

    @Override
    public void run() {
        state.calculate();
        if (state.isCorrect()) {
            printResult();
        }
    }

    private void printResult() {
        System.out.println("Resultat:");
        System.out.println("\ta: " + state.a);
        System.out.println("\tb: " + state.b);
        System.out.println("\tc: " + state.c);
        System.out.println("\td: " + state.d);
        System.out.println("\te: " + state.e);
        System.out.println("\tf: " + state.f);
        System.out.println("\tg: " + state.g);
        System.out.println("\th: " + state.h);
        System.out.println("\ti: " + state.i);
        System.out.println("\tj: " + state.j);
        System.out.println("\tnumber1: " + state.number1);
        System.out.println("\tnumber2: " + state.number2);
        System.out.println("\tnumber3: " + state.number3);
        System.out.println("\tnumber4: " + state.number4);
        System.out.println("\tres1: " + state.res1);
        System.out.println("\tres2: " + state.res2);
        System.out.println("\tres3: " + state.res3);
        System.out.println("\tres4: " + state.res4);
        System.out.println("\tfinres: " + state.finres);
    }
}
