package de.schoolguy.crossnumbers;

public class State {
    int a = 0; // Tradi
    int b = 0; // Multi
    int c = 0; // Mystery
    int d = 0; // Letterbox
    int e = 0; // Event
    int f = 0; // Wherigo
    int g = 0; // Cito
    int h = 0; // Virtual
    int i = 0; // Webcam
    int j = 0; // Earthcache

    int number1 = 0;
    int number2 = 0;
    int number3 = 0;
    int number4 = 0;

    int res1 = 0;
    int res2 = 0;
    int res3 = 0;
    int res4 = 0;

    int finres = 0;

    boolean correct = false;

    State(int a, int b, int c, int d, int e, int f, int g, int h, int i, int j) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
        this.i = i;
        this.j = j;
    }

    void calculate() {
        this.calculateNumbers();
        this.calculateRes();
        this.calculateFinRes();
    }

    private void calculateNumbers() {
        number1 = 100 * d + 10 * b + f;
        number2 = 100 * h + 10 * f + g;
        number3 = 100 * a + 10 * a + h;
        number4 = 100 * h + 10 * d + a;
    }

    private void calculateRes() {
        res1 = 100 * j + 10 * b + g;
        res2 = 10 * d + g;
        res3 = 100 * a + 10 * i + j;
        res4 = 100 * c + 10 * e + h;
    }

    private void calculateFinRes() {
        finres = 100 * e + 10 * g + f;
    }

    public boolean isCorrect() {
        int res1Is = this.number1 + this.number2;
        int res2Is = this.number3 - this.number4;
        int res3Is = this.number1 - this.number3;
        int res4Is = this.number2 + this.number4;

        int endres1 = res1Is - res2Is;
        int endres2 = res3Is + res4Is;

        return endres1 == endres2
                && this.finres == endres1
                && this.res1 == res1Is
                && this.res2 == res2Is
                && this.res3 == res3Is
                && this.res4 == res4Is;
    }
}
