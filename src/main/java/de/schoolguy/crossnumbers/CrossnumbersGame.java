package de.schoolguy.crossnumbers;

import de.schoolguy.Game;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CrossnumbersGame implements Game {

    private long count;
    private ArrayList<State> states;

    public CrossnumbersGame() {
        count = 0;
        states = new ArrayList<>();
    }

    @Override
    public void run() {
        caclulateState();
        this.executeThreads();
    }

    private void caclulateState() {
        for (int a = 0; a <= 9; a++) {
            for (int b = 0; b <= 9; b++) {
                for (int c = 0; c <= 9; c++) {
                    for (int d = 0; d <= 9; d++) {
                        for (int e = 0; e <= 9; e++) {
                            for (int f = 0; f <= 9; f++) {
                                for (int g = 0; g <= 9; g++) {
                                    for (int h = 0; h <= 9; h++) {
                                        for (int i = 0; i <= 9; i++) {
                                            for (int j = 0; j <= 9; j++) {
                                                Set<Integer> numbers = new HashSet<>();
                                                if (numbers.add(a)
                                                        && numbers.add(b)
                                                        && numbers.add(c)
                                                        && numbers.add(d)
                                                        && numbers.add(e)
                                                        && numbers.add(f)
                                                        && numbers.add(g)
                                                        && numbers.add(h)
                                                        && numbers.add(i)
                                                        && numbers.add(j)) {
                                                    State s = new State(a, b, c, d, e, f, g, h, i, j);
                                                    states.add(s);
                                                    count++;
                                                    if (count % 100 == 0 && count != 0) {
                                                        System.out.println("Count at: " + count);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        System.out.println(states.size() + " States calculated.");
    }

    private void executeThreads() {
        ExecutorService executor = Executors.newFixedThreadPool(8);
        for (State s : states) {
            Runnable worker = new Worker(s);
            executor.execute(worker);
        }
        executor.shutdown();
        while (!executor.isTerminated()) {
        }
        System.out.println("Finished all threads");
    }
}
