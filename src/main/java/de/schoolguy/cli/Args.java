package de.schoolguy.cli;

import com.beust.jcommander.Parameter;
import de.schoolguy.GameType;

public class Args {
    @Parameter(names = "--help",
            help = true)
    private boolean help;

    @Parameter(names = {"-t", "--type"},
            description = "Legt fest welches Spiel gestartet wird.",
            required = true,
            converter = GameTypeConverter.class)
    public GameType gameType;
}
