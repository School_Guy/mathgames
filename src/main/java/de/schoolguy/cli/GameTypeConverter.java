package de.schoolguy.cli;

import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.ParameterException;
import de.schoolguy.GameType;

import java.util.Arrays;

public class GameTypeConverter implements IStringConverter<GameType> {

    @Override
    public GameType convert(String value) {
        GameType convertedValue = GameType.fromString(value);

        if (convertedValue == null) {
            throw new ParameterException(
                    String.format("Value %s can not be converted to GameType. Available values are: %s.",
                            value,
                            Arrays.toString(GameType.values())));
        }
        return convertedValue;
    }
}
