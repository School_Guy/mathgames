package de.schoolguy.wheatchessbordproblem;

import de.schoolguy.Game;

import java.math.BigDecimal;

/**
 * This game calculates the numbers of the wheat chessboard game. More information can be found on Wikipedia for this
 * problem: https://en.wikipedia.org/wiki/Wheat_and_chessboard_problem
 */
public class WheatChessboardGame implements Game {

    /**
     * Empty constructor to initialize the game.
     */
    public WheatChessboardGame() {
        // Nothing to init
    }

    /**
     * This calculates the problem in a classical way so that the previous field just get's doubled. Currently this is
     * only possible to 64 because it is hardcoded in this method.
     */
    @Override
    public void run() {
        BigDecimal bigDecimal = new BigDecimal("0");
        for (int i = 0; i < 64; i++) {
            bigDecimal = bigDecimal.add(BigDecimal.valueOf(Math.pow(2, i)));
            System.out.println(i + ": " + bigDecimal);
        }
        System.out.println("Ergebnis: " + bigDecimal.max(bigDecimal));
    }

}
