package de.schoolguy.stringreplacementaddition;

import org.apache.commons.lang3.ArrayUtils;

import java.util.*;

public class Calculator {
    /***
     * An Array with all the summands.
     */
    private Summand[] summands;
    /***
     * The result which is achieved via the summands.
     */
    private Summand result;
    /***
     * This field contains all the letters, from all summands and the result.
     */
    private char[] alphabet;
    /***
     * All Combinations that are possible for calculating the correct solution for the puzzle.
     * Does not contain the solution for the puzzle itself!
     */
    private List<int[]> combinations;

    /***
     * Initializes the calculator.
     * @param array The summands which should be added together.
     * @param result The result which is expected after the add-action happened.
     */
    Calculator(Summand[] array, Summand result) {
        this.summands = array;
        this.result = result;
        combinations = new LinkedList<>();
        init();
        calculatePossibleCombinations();
    }

    /***
     * Initializes the alphabet.
     */
    private void init() {
        Set<Character> characters = new TreeSet<>();
        for (Summand n : summands) {
            char[] line = n.getLineString().toCharArray();
            for (char c : line) {
                characters.add(c);
            }
        }
        char[] line = result.getLineString().toCharArray();
        for (char c : line) {
            characters.add(c);
        }
        Character[] chars = characters.toArray(new Character[characters.size()]);
        alphabet = ArrayUtils.toPrimitive(chars);
        System.out.println("Alphabet: " + Arrays.toString(alphabet));
    }

    /***
     * Calculates all possible combinations. (Via permute)
     */
    private void calculatePossibleCombinations() {
        permute(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9), 0);
        System.out.println(combinations.size());
    }

    /***
     * Recursive calculation of the permutation of a given list of integers,
     * @param arr The values to permute. Must be in lexically order to work correctly.
     * @param k The start index to permute from. If greater than 0, then k values are skipped during the computation.
     */
    private void permute(java.util.List<Integer> arr, int k) {
        for (int i = k; i < arr.size(); i++) {
            Collections.swap(arr, i, k);
            permute(arr, k + 1);
            Collections.swap(arr, k, i);
        }
        if (k == arr.size() - 1) {
            combinations.add(toIntArray(arr));
        }
    }

    /***
     * Converts a Integer-List to a primitive int-array.
     * @param list The list to convert.
     * @return The array with the same order like the list.
     */
    private int[] toIntArray(List<Integer> list) {
        int[] ret = new int[list.size()];
        for (int i = 0; i < ret.length; i++)
            ret[i] = list.get(i);
        return ret;
    }

    /***
     * Creates the calculation threads and runs them afterwards.
     */
    void calculate() {
        List<CalculationThread> threads = new ArrayList<>();

        //Create Threads.
        for (int[] array : combinations) {
            threads.add(new CalculationThread(array, summands, result, alphabet));
        }

        //Run Threads
        for (CalculationThread t : threads) {
            t.run();
        }
    }
}
