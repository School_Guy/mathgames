package de.schoolguy.stringreplacementaddition;

import java.util.Arrays;

public class CalculationThread {
    /***
     * Local field for the summands.
     */
    private Summand[] summands;
    /***
     * Local field for the result.
     */
    private Summand result;
    /***
     * Local field for the alphabet.
     */
    private char[] alphabet;
    /***
     * Local field for the values.
     */
    private int[] values;

    /***
     * The standard constructor for the CalculationThread, only initializes the fields.
     * @param values The values for the letters.
     * @param array The summands which should be afterwards equal to the result.
     * @param result The expected result.
     * @param alphabet The alphabet for the calculation.
     */
    CalculationThread(int[] values, Summand[] array, Summand result, char[] alphabet) {
        this.summands = array;
        this.result = result;
        this.values = values;
        this.alphabet = alphabet;
    }

    /***
     * Method which executes one calculation.
     * If the calculation is correct, then the result is printed to the console.
     */
    public void run() {
        long addResult = 0;
        for (Summand n : summands) {
            n.calculateValue(alphabet, values);
            addResult = addResult + n.getIntValue();
        }
        result.calculateValue(alphabet, values);
        if (addResult == result.getIntValue()) {
            System.out.println("Erfolgreich mit Wert: " + addResult);
            System.out.println(Arrays.toString(values));
            System.out.println(Arrays.toString(alphabet));
            System.out.println("");
        }
    }
}
