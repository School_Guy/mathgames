package de.schoolguy.stringreplacementaddition;

/**
 * This game calculates the correct combination (or combinations) for puzzles which are asking you to replace a letter
 * for a digit and therefore matching an expected result.
 */
public class StringReplacementGame implements de.schoolguy.Game {

    /**
     * Constructor to initialize things for this game.
     */
    public StringReplacementGame() {
        // Nothing to init
    }

    /**
     * This methods start the calculation for this game. The values for the replacement are currently hardcoded into
     * this method.
     */
    @Override
    public void run() {
        Summand line1 = new Summand("ABCDEF");
        Summand line2 = new Summand("GIACGG");
        Summand line3 = new Summand("DBCHHH");
        Summand result = new Summand("DGBFAGD");
        Calculator calculator = new Calculator(new Summand[]{line1, line2, line3}, result);
        calculator.calculate();
    }
}
