package de.schoolguy.stringreplacementaddition;

import org.apache.commons.lang3.ArrayUtils;

public class Summand {
    /***
     * Contains the string representation of this summand.
     */
    private String line;
    /***
     * Contains the number which is represented with the current values for the alphabet. May be not the current one.
     * The value '0' indicates that the calculation for the term was not run.
     */
    private long number;

    /***
     * Creates a new summand and initializes the private fields.
     * @param line
     */
    Summand(String line) {
        setLine(line);
    }

    /***
     * Sets the private field line to a new value.
     * @param line The new value for the field line.
     */
    private void setLine(String line) {
        this.line = line;
        number = 0;
    }

    /***
     * Getter for the private field number.
     * @return Returns the current calculated number for the string.
     */
    long getIntValue() {
        return number;
    }

    /***
     * Gets the current string.
     * @return The string which is currently set.
     */
    String getLineString() {
        return line;
    }

    /***
     * Method which calculates the value of the string.
     * @param alphabet The alphabet, if index is negative, then the puzzle is not solvable.
     * @param value The values for the letters, index 0 corresponds to index 0 at the alphabet array.
     */
    void calculateValue(char[] alphabet, int[] value) {
        number = 0;
        char[] lineArray = line.toCharArray();
        ArrayUtils.reverse(lineArray);
        for (int i = 0; i < lineArray.length; i++) {
            int index = -1;
            for (int j = 0; j < alphabet.length; j++) {
                if (lineArray[i] == alphabet[j]) {
                    index = j;
                    break;
                }
            }
            if (index < 0) {
                throw new IllegalArgumentException("Character is not in the alphabet. Puzzle not solvable!");
            }
            Double tmp = Math.pow(10, i) * value[index];
            number = number + tmp.intValue();
        }
    }
}
