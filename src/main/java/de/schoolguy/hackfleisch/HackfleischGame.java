package de.schoolguy.hackfleisch;

import de.schoolguy.Game;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;

/**
 * This Game calculates all hashes in a in the code specified format. The hash method is currently MD5.
 */
public class HackfleischGame implements Game {

    /**
     * This initializes the game for calculation.
     */
    public HackfleischGame() {
        // Nothing to init
    }

    /**
     * This calculates the coordinates for the following format:
     * n aa bb.ccc
     * e aa bb.ccc
     *
     * Import is that the N and E are in lowercase because otherwise the hash would not match.
     *
     * The charset with which the message is decoded is "8859_1".
     */
    @Override
    public void run() {

        try {
            String text = "";
            MessageDigest md = MessageDigest.getInstance("MD5");
            DecimalFormat df = new DecimalFormat("000");

            for (int i = 42; i <= 45; i++) {
                for (int j = 0; j < 1000; j++) {
                    text = "n 48 " + i + "." + df.format(j);
                    byte[] theTextToDigestAsBytes = text.getBytes("8859_1");
                    md.update(theTextToDigestAsBytes);
                    byte[] digest = md.digest();

                    //dump out the hash
                    for (byte b : digest) {
                        System.out.printf("%02X", b & 0xff);
                    }
                    System.out.println(" " + text + "\n");
                }

            }

            System.out.println("--------------------------------");

            for (int i = 24; i <= 29; i++) {
                for (int j = 0; j < 1000; j++) {
                    text = "e 11 " + i + "." + df.format(j);
                    byte[] theTextToDigestAsBytes = text.getBytes("8859_1");
                    md.update(theTextToDigestAsBytes);
                    byte[] digest = md.digest();

                    //dump out the hash
                    for (byte b : digest) {
                        System.out.printf("%02X", b & 0xff);
                    }
                    System.out.println(" " + text + "\n");
                }

            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }
}
