# How to contribute?

This is an OpenSource repository! Feel free to fork, develop in a feature branch
and then open a merge-request.

I will most likely accept any reasonable merge-request which improves the
quality of the project, be it speed, docs or readability.
