# MathGames

Each problem is living in his own package. Currently, there is no way of just grabbing an executable and then saying
"solve".

## Computewall

Program to find a special solution to a mathematical problem called "Zahlenmauer" which is german and translates
roughly to number-wall.

So what is the actual problem?

Imagine as an example the following structure:

```
        |_0_|
      |_0_|_0_|
    |_0_|_0_|_0_|
|_0_|_0_|_0_|_0_|
| 0 | 0 | 0 | 0 | 0 |
```

Where the number 0 stands for any number which can be calculated by adding/subtracting/multiplying/dividing two numbers
below your chosen number. The numbers in the bottom row can be chosen to your liking.

The difficulty is chosen by the one who is replacing the zeros in the puzzle and choosing the arithmetic operation.
The bottom row can have any amount of numbers where the operation %2 doesn't return zero, so any odd number.

## Crossnumbers

This game currently takes no arguments, and the exact purpose is unknown.

## Hackfleisch

This package solves the geocache [GC2JBHA](https://coord.info/GC2JBHA). There is currently no flexibility to solve
caches similar to this one.

## MaxSubString

This algorithm takes two arguments: String z1 and String z2. The algorithm then computes the longest substring which is
contained in both strings.

In the project there are two versions of this algorithm: an iterative and a recursive one.

If there are multiple longest strings, my implementation only returns the first one found.

## Unknown

This game takes a nine-digit number and divides it by a three-digit number the result needs to be a six-digit number
which then gets divided through a two-digit number. The final result need to be a five-digit number.

All steps need to be divisions without any fractions of a number. 

This game takes no arguments currently. It is running linear.

## Wheat-Chessboard-Problem

Print all numbers from the first field to field 64 on the console.

## String Replacement Addition

This game calculates the correct combination (or combinations) for puzzles which are asking you to replace a letter
for a digit and therefore matching an expected result.
